
window.onload = function(){
    checkAuth();
    setInterval(function(){checkAuth()}, 10000); 
};
window.onblur = function(){userLogout();};

/****************
<script type="text/javascript" src="js/json2.js"></script>
<script type="text/javascript" src="js/l10n.min.js"></script>
<script type="text/javascript" src="js/underscore.js"></script>
<script type="text/javascript" src="js/localizations.js"></script>
var localize = function(string, fallback) {
    var localized = string.toLocaleString();
    return (localized === string) ? fallback : localized;
};

var t = function(string){
    var e = document.getElementById(string);
    if( e ){
        e = e.firstChild;
        e.nodeValue = localize('%'+string, e.nodeValue);
    }
}
    //String.toLocaleString("localizations.json");
    //String.locale = "tr";
    //String.locale = "en";
    //t('login');
    //t('changePassword');
    //document.documentElement.lang = String.locale || document.documentElement.lang;
*****************/

function checkAuth(){
    var c = getCookie('citadelAuth');
    if(c != 1){
        var z = document.getElementById("divLogin");
        if(!z){
            var e = document.getElementById("content");
            e.innerHTML = document.getElementById("template_Login").innerHTML;
        }
    }else{
        var z = document.getElementById("divUser");
        if(!z){
            var e = document.getElementById("content");
            e.innerHTML = document.getElementById("template_User").innerHTML;        
        }
    }
}

function userLogout(){
    delCookie('citadelAuth');
    delCookie('PHPSESSID');
    checkAuth();
}

function Ajax(params){
    var ajax = new XMLHttpRequest();
    ajax.onreadystatechange=function(){
        if( ajax.readyState==4 && ajax.status==200 ){
            if( ajax.responseText != '' ){
                Messanger(ajax.responseText);                    
            }else{
                Messanger('',-1);                    
            }
        }
    };
    ajax.open("POST", "ajax.php", true);
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.setRequestHeader("Content-length", params.length);
    ajax.setRequestHeader("Connection", "close");
    ajax.send(params);    
}

var l = function (string) {
    return string.toLocaleString();
};

function userLogin(){
    var f = document.getElementById("FormLogin");
    var p = document.getElementById("pass");

    var data = formData(f);

    if (data.user.length < 3) {
        Messanger("Username is to short");
    }else if (data.pass.length < 4) {
        Messanger("Password is to short");
    }else{
        Messanger("Checking server ...",1);

        var params = "a=Login&u=" + data.user + "&p=" + data.pass ;
        Ajax(params);
    }
    p.value = "";

    var content = document.getElementById("content");
    content.innerHTML = document.getElementById("template_User").innerHTML;
}

function userPassword(){
    var f = document.getElementById("FormPassword");
    var p1 = document.getElementById("pass1");
    var p2 = document.getElementById("pass2");
    var data = formData(f);

    if (data.pass1.length < 3) {
        Messanger("Username is to short");
    }else if (data.pass1 != data.pass2) {
        Messanger("Passwords don't match");
    }else{
        Messanger("Checking server ...",1);

        var params = "a=Password&p=" + data.pass1 ;
        Ajax(params);
    }
    p1.value = "";
    p2.value = "";
}


function clearText(thefield){
    if (thefield.defaultValue==thefield.value)
    thefield.value = ""
}

function Messanger(text, no){
    var e = document.getElementById("messanger");
    e.innerText = e.textContent = text;

    if (no === -1) {
        e.style.display = 'none';
        return
    }

    if (no === 1) {
        e.className = "info";
    }else{
        e.className = "alert";
    }
    e.style.display = 'block';
    setTimeout(function(){Messanger('',-1)},text.length * 100 + 3000);
}

function formData(form){
    var r = {};
    var i = 0;
    for (i = 0; i < form.length; ++i) {
        if (form[i] !== undefined) {
            if (form[i].name) {
                field_type = form[i].type.toLowerCase();
                switch (field_type) {
                    case "text":
                        r[form[i].name] = form[i].value;
                        break;
                    case "password":
                        r[form[i].name] = form[i].value;
                        break;
                    case "textarea":
                        r[form[i].name] = form[i].value;
                        break;
                    case "radio":
                    case "checkbox":
                        r[form[i].name] = form[i].checked;
                        break;
                    case "select-one":
                        r[form[i].name] = form[i].value;
                    case "select-multi":
                        r[form[i].name] = form[i].selectedIndex;
                        break;
                    default:
                        break;
                }
            }
        }
    }
    return r;
}

function setCookie(name,value,days) {
    var expires = "";
    if( days ){
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    } 
    document.cookie = name+"="+value+expires+"; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') 
            c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) 
            return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function delCookie(name) {
    setCookie(name,"",-1);
}

function isString(x) {
  return (typeof x === 'string');
}
