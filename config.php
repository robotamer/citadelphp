<?php defined('CITADEL_PHP') || exit("[ERROR] No direct access!");
#### DO NOT CHANGE THE TOP LINE! ####

/**
 * Coder Notes:
 * @todo Convert config.php to an ini file
 **/

#### CONFIG OPTIONS ARE BELOW! ####

/**
 * CITADEL_SOCKET
 * Find the citadel.socket on your server. 
 * Possible known values are:
 *    /var/run/citadel/citadel.socket
 *    /usr/local/citadel/citadel.socket
 **/
defined('CITADEL_SOCKET')   || define('CITADEL_SOCKET'  , '/var/run/citadel/citadel.socket');
# defined('CITADEL_SOCKET')   || define('CITADEL_SOCKET'  , '/usr/local/citadel/citadel.socket');

/**
 * CITADEL_TIMEZONE
 **/
defined('CITADEL_TIMEZONE') || define('CITADEL_TIMEZONE', 'America/Los_Angeles');
?>