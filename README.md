Citadel PHP
===========

[Citadel] is a email and collaboration server.

There is also a web server for [Citadel] called Webcit.
However, I use Webcit almost exclusively for Admin/Aida purposes.

My users use either a desktop client or Roundcube, which is a PHP driven web mail client.
________________________________________________________

### PURPOSE

The purpose of Citadel PHP is to give users a means to change their passwords. 
The server side protocols IMAP, SMTP etc. don't provide any means to change passwords.
________________________________________________________

### INCLUDES

 - A library to access [Citadel] email and collaboration servers from php, using the [Citadel] Protocol.
 - A simple ajax driven website on which users can:
     + Login 
     + Change Password 
________________________________________________________

### INSTALL  

Installing is as simple as:

 1. Clone or download the code to your web root
 2. Modify config.php
 3. View on https://www.example.com/citadelphp
________________________________________________________

### CONFIG  

#### CITADEL_SOCKET

Find the citadel.socket on your server. Possible values are:

 - `'/var/run/citadel/citadel.socket'`
 - `'/usr/local/citadel/citadel.socket'`
________________________________________________________

### CREDITS  

##### Baby Citadel @author Warren Stevens

There is another php library for Citadel called Baby Citadel.
I first tried impleamenting this with that library, but after 
having to make too many changes to the origianl code I decided 
to write a new library from scratch. Some of the implementation 
ideas for **Citadel PHP** come from **Baby Citadel**.
________________________________________________________

### LICENSE

#### The MIT License (MIT)

Copyright © 2014 Dennis T Kaplan <http://www.robotamer.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


[Citadel]:(http://www.citadel.org "Citadel")

