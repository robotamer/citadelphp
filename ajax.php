<?php
defined('CITADEL_PHP') || define('CITADEL_PHP', TRUE);


session_start();
include('config.php');
include_once('Citadel.php');

$cit = new Citadel();
$cit->Socket(CITADEL_SOCKET);
$cit->Timeout(1);
$cit->Open();
$cit->Identify(12, 3, 1, "CitadelPHP Password");

if( !isset($_REQUEST["a"]) ){
	echo 'No Action';	
}elseif( isset($_REQUEST["email"]) && !empty($_REQUEST["email"])){
	echo 'Please contact the webmaster!';
}elseif($_REQUEST["a"] == 'Login'){
	$username=$_REQUEST["u"];
	$password=$_REQUEST["p"];
	$_SESSION = $cit->Login($username, $password);
	if( !empty($_SESSION) ) {
		$_SESSION['Authenticated'] = TRUE;
		$_SESSION['password'] = $password;
	    setcookie("citadelAuth", 1, time()+3600, '/');
	} else {
	    echo "Login: " . $cit->Status();
	}	
}elseif($_REQUEST["a"] == 'Password') {

	$cit->Login($_SESSION['username'], $_SESSION['password']);

	$password=$_REQUEST["p"];

	if(strlen($password) < 8){
		echo 'Password is too short ';
	}elseif($cit->Password($password)){
	    echo "Password Successfull Changed";
	} else {
	    echo "Password: " . $cit->Status();
	}	
}


?>
